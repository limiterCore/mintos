import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import store from 'store/store';

// Components
/* eslint-disable import/no-named-as-default */
import App from 'components/App';
import Login from 'components/Login';
import Registration from 'components/Registration';
/* eslint-enable import/no-named-as-default */
import PrivateComponent from 'HOC/PrivateComponent';
import PublicComponent from 'HOC/PublicComponent';

// Actions
import { getUserInfo } from 'store/actions';

// Styles
import './index.scss';

store.dispatch(getUserInfo()).catch(() => {});

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={PrivateComponent(App)} />
        <Route path="/login" component={PublicComponent(Login)} />
        <Route path="/register" component={PublicComponent(Registration)} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
