import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Components
import Loading from 'components/Loading';

const PrivateComponent = Component => {
  const Auth = ({ gotUser, isUserAuth, ...props }) => {
    if (!gotUser) {
      return <Loading />;
    }

    if (!isUserAuth) {
      return <Redirect to="/login" />;
    }

    return <Component {...props} />;
  };

  Auth.propTypes = {
    gotUser: PropTypes.bool.isRequired,
    isUserAuth: PropTypes.bool.isRequired,
  };

  const mapStateToProps = state => ({
    gotUser: state.gotUser,
    isUserAuth: state.isUserAuth,
  });

  return connect(mapStateToProps)(Auth);
};

export default PrivateComponent;
