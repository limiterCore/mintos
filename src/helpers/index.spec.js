import { getMostFrequentWords } from './index';

test('getMostFrequentWords should work correctly without top 50 english words', () => {
  expect(getMostFrequentWords('Man Car goes car goes car banana')).toEqual([
    {
      word: 'car',
      frequency: 3,
    },
    {
      word: 'goes',
      frequency: 2,
    },
    {
      word: 'man',
      frequency: 1,
    },
    {
      word: 'banana',
      frequency: 1,
    },
  ]);
  expect(getMostFrequentWords('')).toEqual([]);
  expect(getMostFrequentWords('test')).toEqual([
    {
      word: 'test',
      frequency: 1,
    },
  ]);
  expect(getMostFrequentWords(' ')).toEqual([]);
  expect(getMostFrequentWords('Spa" SPA')).toEqual([
    {
      word: 'spa',
      frequency: 2,
    },
  ]);
});

test('getMostFrequentWords should work correctly with top 50 english words and punctuation', () => {
  expect(
    getMostFrequentWords(
      'the Man goes to a Car in order to get a food from a car. Man just wants to eat.',
      'Man goes Car order food car. Man just wants eat.',
    ),
  ).toEqual([
    {
      word: 'man',
      frequency: 2,
    },
    {
      word: 'car',
      frequency: 2,
    },
    {
      word: 'goes',
      frequency: 1,
    },
    {
      word: 'order',
      frequency: 1,
    },
    {
      word: 'food',
      frequency: 1,
    },
    {
      word: 'just',
      frequency: 1,
    },
    {
      word: 'wants',
      frequency: 1,
    },
    {
      word: 'eat',
      frequency: 1,
    },
  ]);
});

test('getMostFrequentWords should work correctly with edge cases', () => {
  expect(getMostFrequentWords(0)).toEqual([]);
  expect(getMostFrequentWords(1)).toEqual([
    {
      word: '1',
      frequency: 1,
    },
  ]);
  expect(getMostFrequentWords(false)).toEqual([]);
  expect(getMostFrequentWords(null)).toEqual([]);
  expect(getMostFrequentWords(undefined)).toEqual([]);
  expect(getMostFrequentWords({})).toEqual([]);
});
