import { top50words } from '../constants';

export const handleFetchErrors = async res =>
  Promise.resolve({
    ok: res.ok,
    statusText: res.statusText,
    status: res.status,
    response: await res.text(),
  });

export const handleJSON = res => {
  if (res.ok) {
    try {
      return JSON.parse(res.response);
    } catch (e) {
      return res.response;
    }
  } else {
    let toThrow = null;
    try {
      toThrow = JSON.parse(res.response);
    } catch (e) {
      toThrow = Error(res.statusText || res.status);
    }
    throw toThrow;
  }
};

// Получаем куку по имени
export function getCookie(name) {
  const matches = document.cookie.match(
    new RegExp(`(?:^|; )${name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1')}=([^;]*)`),
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

export const fetchWrapper = (
  url,
  { method = 'GET', csrfToken = null, body = null, needToken = false, json = true } = {},
) => {
  if (!url) {
    throw new Error('Url should be specified');
  }
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  if (csrfToken) {
    headers['X-CSRFToken'] = csrfToken;
  }

  if (needToken && window.localStorage.getItem('jwt')) {
    headers.Authorization = `Token ${window.localStorage.getItem('jwt')}`;
  }

  const options = {
    method,
    headers,
    credentials: 'include',
  };

  if (body) {
    options.body = body;
  }

  return fetch(url, options)
    .then(handleFetchErrors)
    .then(res => {
      if (json) {
        return handleJSON(res);
      }
      return res;
    });
};

export const debounce = (func, ms) => {
  let timer = null;

  return function inner(...args) {
    if (timer) {
      clearTimeout(timer);
    }

    return new Promise(resolve => {
      const onComplete = () => {
        const result = func.apply(this, args);
        timer = null;
        resolve(result);
      };

      timer = setTimeout(onComplete, ms);
    });
  };
};

export const validateEmail = string => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(string).toLowerCase());
};

/** Get's a single child element by tag name */
export const getTag = (entry, tagName) => {
  return entry.getElementsByTagName(tagName) ? entry.getElementsByTagName(tagName)[0] : null;
};

/** Get date from an entry element */
export const getDate = entry => {
  const tag = getTag(entry, 'updated');
  if (tag && tag.textContent && Date.parse(tag.textContent)) {
    return new Date(tag.textContent);
  }
  return new Date();
};

/** Strip html tags to remove html tags like b, i, h1, etc. */
export const html2txt = html => html.replace(/<(?:.|\n)*?>/gm, '');

/** Get most frequent words */
export const getMostFrequentWords = content => {
  const countedWords = {};

  if (!content) {
    return [];
  }

  if (typeof content === 'object') {
    return [];
  }

  return content
    .toString()
    .toLowerCase()
    .replace(/[^\w\s]|_/g, '')
    .replace(/[\s]+/g, ' ')
    .split(' ')
    .filter(item => item)
    .map((item, index, self) => {
      if (top50words.includes(item)) {
        return null;
      }
      if (!countedWords[item]) {
        countedWords[item] = self.filter(word => word === item).length;
        return {
          word: item,
          frequency: countedWords[item],
        };
      }
      return null;
    })
    .filter(item => item)
    .sort((a, b) => b.frequency - a.frequency);
};
