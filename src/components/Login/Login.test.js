import React from 'react';
import { render, cleanup } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Login } from './index';

afterEach(cleanup);

it('renders without crashing', () => {
  render(
    <MemoryRouter>
      <Login dispatch={() => {}} />
    </MemoryRouter>,
  );
});

it('renders disabled button by default', () => {
  const dispatch = jest.fn(() => Promise.resolve());
  const { getAllByText } = render(
    <MemoryRouter>
      <Login dispatch={dispatch} />
    </MemoryRouter>,
  );

  expect(getAllByText('Login')[1]).toHaveProperty('disabled', true);
});
