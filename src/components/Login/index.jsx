import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

// Components
import Form from 'components/Form';
import FormInputHolder from 'components/FormInputHolder';
import FormButton from 'components/FormButton';

// Actions
import { login } from 'store/actions';

// Misc
import { validateEmail } from 'helpers';

// Styles
import './styles.scss';

export const Login = ({ dispatch }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState({});
  const [redirectURL, setRedirectURL] = useState(null);

  useEffect(() => {
    if (!email || !password || !validateEmail(email)) {
      setIsButtonDisabled(true);
    } else {
      setIsButtonDisabled(false);
    }
  }, [email, password]);

  const sendForm = () => {
    setIsLoading(true);
    setErrors({});
    dispatch(login(email, password))
      .then(() => {
        setRedirectURL('/');
      })
      .catch(err => {
        setIsLoading(false);
        if ('errors' in err) {
          setErrors(err.errors);
        } else {
          setErrors({
            an: 'error occurred',
          });
        }
      });
  };

  /** Redirecting after successful login */
  if (redirectURL) {
    return <Redirect to={redirectURL} />;
  }

  return (
    <div className="login">
      <div className="login__container">
        <h1 className="login__title">Login</h1>

        <Form>
          <FormInputHolder
            onChange={({ target }) => {
              setEmail(target.value);
            }}
            value={email}
            placeholder="E-mail"
            disabled={isLoading}
          />

          <FormInputHolder
            onChange={({ target }) => {
              setPassword(target.value);
            }}
            value={password}
            placeholder="Password"
            type="password"
            disabled={isLoading}
          />

          {/* ERRORS */}
          {Object.keys(errors).length > 0 && (
            <div className="form__errors">
              {Object.keys(errors).map(key => (
                <div className="form__error">{`${key} ${errors[key]}`}</div>
              ))}
            </div>
          )}
          {/* /ERRORS */}

          <FormButton
            onClick={sendForm}
            disabled={isButtonDisabled}
            isLoading={isLoading}
            data-testid="login-button"
          >
            Login
          </FormButton>

          <div className="login__reg">
            Do not have an account yet?
            <Link className="login__reg-link" to="/register">
              Sign up here
            </Link>
          </div>
        </Form>
      </div>
    </div>
  );
};

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(Login);
