import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import './styles.scss';

const Form = ({ className, children }) => {
  return (
    <div
      className={cx('form', {
        [className]: className,
      })}
    >
      {children}
    </div>
  );
};

Form.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

Form.defaultProps = {
  className: null,
};

export default Form;
