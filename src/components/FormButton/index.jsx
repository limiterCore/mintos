import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

// Icons
import { ReactComponent as IconLoader } from 'assets/icons/icon-loader.svg';

// Styles
import './styles.scss';

const FormButton = ({ className, onClick, disabled, children, isLoading }) => {
  return (
    <button
      type="button"
      className={cx('form-button', {
        [className]: className,
      })}
      onClick={onClick}
      disabled={disabled || isLoading}
    >
      {children}
      {isLoading && <IconLoader className="form-button__loading" />}
    </button>
  );
};

FormButton.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  children: PropTypes.node,
  isLoading: PropTypes.bool,
};

FormButton.defaultProps = {
  className: null,
  children: null,
  disabled: false,
  isLoading: false,
};

export default FormButton;
