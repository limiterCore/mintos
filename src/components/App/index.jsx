import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Actions
import { getFeed, logout } from 'store/actions';

// Styles
import './styles.scss';

export const App = ({ feed, mostFrequentWords, dispatch }) => {
  const [redirectURL, setRedirectURL] = useState(null);

  /** Get feed and all content at initial load */
  useEffect(() => {
    dispatch(getFeed());
  }, [dispatch]);

  const onLogoutClick = () => {
    dispatch(logout())
      .then(() => {
        setRedirectURL('/login');
      })
      .catch(() => {});
  };

  if (redirectURL) {
    return <Redirect to={redirectURL} />;
  }

  return (
    <div className="app">
      <div className="app__container">
        <div className="app__header">
          <h1 className="app__title">News Feed</h1>
          <button type="button" onClick={onLogoutClick} className="app__header-logout">
            Log out
          </button>
        </div>

        {mostFrequentWords.length > 0 && (
          <div className="app__words">
            <p className="app__words-title">10 Most frequent words from feed:</p>
            <div className="app__words-list">
              {mostFrequentWords.slice(0, 10).map(item => (
                <div className="app__words-item">
                  <span className="app__words-item-frequency">{item.frequency}</span>
                  <span className="app__words-item-word">{item.word}</span>
                </div>
              ))}
            </div>
          </div>
        )}

        <div className="app__list">
          {feed.map(item => {
            return (
              <div className="app__item" key={item.id}>
                <p className="app__item-date">{item.date}</p>
                <h3 className="app__item-title">
                  <a
                    href={item.href}
                    className="app__item-link"
                    target="_blank"
                    rel="noreferrer noopener"
                  >
                    {item.title}
                  </a>
                </h3>

                {item.content && <div className="app__item-content">{`${item.content}`}</div>}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

App.propTypes = {
  feed: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      content: PropTypes.string,
      date: PropTypes.string,
      href: PropTypes.string,
      title: PropTypes.string,
    }),
  ).isRequired,
  dispatch: PropTypes.func.isRequired,
  mostFrequentWords: PropTypes.arrayOf(
    PropTypes.shape({
      word: PropTypes.string,
      frequency: PropTypes.number,
    }),
  ).isRequired,
};

const mapStateToProps = state => ({
  feed: state.feed,
  mostFrequentWords: state.mostFrequentWords,
});

export default connect(mapStateToProps)(App);
