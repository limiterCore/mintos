import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import { App } from './index';

afterEach(cleanup);

it('renders without crashing', () => {
  render(<App feed={[]} mostFrequentWords={[]} dispatch={() => {}} />);
});

it('calls dispatch when constructing and on the button click', () => {
  const dispatch = jest.fn(() => Promise.resolve());
  const { getByText } = render(<App feed={[]} mostFrequentWords={[]} dispatch={dispatch} />);

  /** Initial dispatch for the data */
  expect(dispatch).toHaveBeenCalledTimes(1);

  /** Dispatch for the log out */
  fireEvent.click(getByText('Log out'));
  expect(dispatch).toHaveBeenCalledTimes(2);
});
