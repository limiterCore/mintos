import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

// Components
import Form from 'components/Form';
import FormInputHolder from 'components/FormInputHolder';
import FormButton from 'components/FormButton';

// Actions
import { register, checkEmail } from 'store/actions';

// Misc
import { debounce, validateEmail } from 'helpers';

// Styles
import './styles.scss';

export const Registration = ({ dispatch }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState({});
  const [redirectURL, setRedirectURL] = useState(null);
  const [isEmailExist, setIsEmailExist] = useState(false);
  const [isEmailChecked, setIsEmailChecked] = useState(false);

  /** Create debounced callback for checking email */
  const debouncedCallback = useRef();
  useEffect(() => {
    debouncedCallback.current = debounce(localEmail => checkEmail(localEmail), 400);
  }, []);

  /** Check email and password as they change */
  useEffect(() => {
    if (!email || !password || !validateEmail(email)) {
      setIsButtonDisabled(true);
    } else {
      setIsButtonDisabled(false);
    }
  }, [email, password]);

  /** Check email as it changes (debounced) */
  useEffect(() => {
    setIsEmailChecked(false);
    debouncedCallback
      .current(email)
      .then(res => {
        setIsEmailChecked(true);
        setIsEmailExist(res.result);
      })
      .catch(() => {});
  }, [email]);

  /** Function for sending form */
  const sendForm = () => {
    setIsLoading(true);
    dispatch(register(email, password))
      .then(() => {
        setRedirectURL('/');
      })
      .catch(err => {
        setIsLoading(false);
        if ('errors' in err) {
          setErrors(err.errors);
        } else {
          setErrors({
            an: 'error occurred',
          });
        }
      });
  };

  /** Redirecting after successful login */
  if (redirectURL) {
    return <Redirect to={redirectURL} />;
  }

  return (
    <div className="registration">
      <div className="registration__container">
        <h1 className="registration__title">Registration</h1>

        <Form>
          <FormInputHolder
            onChange={({ target }) => {
              setEmail(target.value);
            }}
            value={email}
            placeholder="E-mail"
            disabled={isLoading}
            error={isEmailExist ? 'Such e-mail has been already registered' : null}
          />

          <FormInputHolder
            onChange={({ target }) => {
              setPassword(target.value);
            }}
            value={password}
            placeholder="Password"
            type="password"
            disabled={isLoading}
          />

          {/* ERRORS */}
          {Object.keys(errors).length > 0 && (
            <div className="form__errors">
              {Object.keys(errors).map(key => (
                <div className="form__error">{`${key} ${errors[key]}`}</div>
              ))}
            </div>
          )}
          {/* /ERRORS */}

          <FormButton
            onClick={sendForm}
            disabled={isButtonDisabled || !isEmailChecked || isEmailExist}
            isLoading={isLoading}
          >
            Register
          </FormButton>

          <div className="registration__login">
            Already have an account?
            <Link className="registration__login-link" to="/login">
              Login here
            </Link>
          </div>
        </Form>
      </div>
    </div>
  );
};

Registration.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(Registration);
