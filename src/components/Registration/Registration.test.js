import React from 'react';
import { render, cleanup } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { Registration } from './index';

afterEach(cleanup);

it('renders without crashing', () => {
  render(
    <MemoryRouter>
      <Registration dispatch={() => {}} />
    </MemoryRouter>,
  );
});

it('renders disabled button by default', () => {
  const dispatch = jest.fn(() => Promise.resolve());
  const { getByText } = render(
    <MemoryRouter>
      <Registration dispatch={dispatch} />
    </MemoryRouter>,
  );

  expect(getByText('Register')).toHaveProperty('disabled', true);
});
