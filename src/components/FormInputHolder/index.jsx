import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

import './styles.scss';

const FormInputHolder = ({ className, value, onChange, placeholder, type, error }) => {
  return (
    <div
      className={cx('form-input-holder', {
        [className]: className,
      })}
    >
      <input
        className="form-input-holder__input"
        type={type}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
      />

      {error && <p className="form-input-holder__error">{error}</p>}
    </div>
  );
};

FormInputHolder.propTypes = {
  className: PropTypes.string,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  error: PropTypes.string,
};

FormInputHolder.defaultProps = {
  className: null,
  placeholder: '',
  type: 'text',
  error: null,
};

export default FormInputHolder;
