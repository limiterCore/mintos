import React from 'react';

import { ReactComponent as IconLoader } from 'assets/icons/icon-loader.svg';

import './styles.scss';

const Loading = () => (
  <div className="loading">
    <IconLoader className="loading__icon" />
  </div>
);

export default Loading;
