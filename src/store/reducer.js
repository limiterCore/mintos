import * as types from 'store/action-types';

const initialState = {
  user: null,
  isUserAuth: false,
  gotUser: false,
  feed: [],
  mostFrequentWords: [],
};

const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_USER_SUCCESS: {
      window.localStorage.setItem('jwt', action.payload.user.token);
      return {
        ...state,
        user: action.payload.user,
        gotUser: true,
        isUserAuth: true,
      };
    }

    case types.GET_USER_FAIL:
      return {
        ...state,
        gotUser: true,
        isUserAuth: false,
      };

    case types.LOGIN_SUCCESS: {
      window.localStorage.setItem('jwt', action.payload.user.token);
      return {
        ...state,
        user: action.payload.user,
        isUserAuth: true,
      };
    }

    case types.LOGOUT_SUCCESS: {
      window.localStorage.setItem('jwt', '');
      return {
        ...state,
        user: {},
        isUserAuth: false,
      };
    }

    case types.REGISTER_SUCCESS: {
      window.localStorage.setItem('jwt', action.payload.user.token);
      return {
        ...state,
        user: action.payload.user,
        isUserAuth: true,
      };
    }

    case types.GET_FEED_SUCCESS:
      return {
        ...state,
        feed: action.payload.feed,
        mostFrequentWords: action.payload.mostFrequentWords,
      };

    default:
      return state;
  }
};

export default mainReducer;
