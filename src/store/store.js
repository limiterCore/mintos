import { createStore, applyMiddleware, compose } from 'redux';
import mainReducer from 'store/reducer';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

/** Creating the logger for development */
const loggerMiddleware = createLogger({
  duration: true,
  diff: true,
  collapsed: true,
});

/** Compose function */
let composeEnhancers = compose;

/** Compose function for development */
if (process.env.NODE_ENV === 'development') {
  /* eslint-disable no-underscore-dangle */
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  /* eslint-enable */
}

/** Creating middlewares */
const middlewares = [thunk];

/** Add logger for development */
if (process.env.NODE_ENV === 'development') {
  middlewares.push(loggerMiddleware);
}

export default createStore(mainReducer, composeEnhancers(applyMiddleware(...middlewares)));
