import { fetchWrapper, getDate, getMostFrequentWords, getTag, html2txt } from 'helpers';
import * as types from 'store/action-types';

export const getUserInfo = () => dispatch => {
  const url = `${process.env.REACT_APP_API_URL}users/current`;

  return new Promise((resolve, reject) => {
    fetchWrapper(url, {
      needToken: true,
    })
      .then(res => {
        dispatch({
          type: types.GET_USER_SUCCESS,
          payload: {
            user: (res && res.user) || {},
          },
        });
        resolve(res);
      })
      .catch(res => {
        dispatch({
          type: types.GET_USER_FAIL,
          payload: {
            res,
          },
        });
        reject(res);
      });
  });
};

export const login = (email, password) => dispatch => {
  const url = `${process.env.REACT_APP_API_URL}users/login`;

  return new Promise((resolve, reject) => {
    fetchWrapper(url, {
      method: 'POST',
      body: JSON.stringify({
        user: {
          email,
          password,
        },
      }),
    })
      .then(res => {
        dispatch({
          type: types.LOGIN_SUCCESS,
          payload: {
            user: (res && res.user) || {},
          },
        });
        resolve(res);
      })
      .catch(res => {
        dispatch({
          type: types.LOGIN_FAIL,
          payload: {
            res,
          },
        });
        reject(res);
      });
  });
};

export const logout = () => dispatch => {
  dispatch({
    type: types.LOGOUT_SUCCESS,
  });
  return Promise.resolve();
};

export const register = (email, password) => dispatch => {
  const url = `${process.env.REACT_APP_API_URL}users`;

  return new Promise((resolve, reject) => {
    fetchWrapper(url, {
      method: 'POST',
      body: JSON.stringify({
        user: {
          email,
          password,
        },
      }),
    })
      .then(res => {
        dispatch({
          type: types.REGISTER_SUCCESS,
          payload: {
            user: (res && res.user) || {},
          },
        });
        resolve(res);
      })
      .catch(res => {
        dispatch({
          type: types.REGISTER_FAIL,
          payload: {
            res,
          },
        });
        reject(res);
      });
  });
};

export const checkEmail = email => {
  const url = `${process.env.REACT_APP_API_URL}users/check`;

  return fetchWrapper(url, {
    method: 'POST',
    body: JSON.stringify({
      email,
    }),
  });
};

export const getFeed = () => dispatch => {
  const url = `${process.env.REACT_APP_API_URL}feed`;

  return new Promise((resolve, reject) => {
    fetchWrapper(url, {
      needToken: true,
    })
      .then(res => {
        const parser = new DOMParser();
        try {
          const xmlDoc = parser.parseFromString(res.feed, 'text/xml');

          if (xmlDoc instanceof XMLDocument) {
            const entries = xmlDoc.getElementsByTagName('entry');
            let allContent = '';

            /** Map entries on order to obtain content */
            const feed = Array.from(entries)
              .map(item => {
                const date = getDate(item);
                const day = date
                  .getDate()
                  .toString()
                  .padStart(2, '0');
                const month = (date.getMonth() + 1).toString().padStart(2, '0');
                const dateString = `${day}.${month}.${date.getFullYear()}`;

                const content = getTag(item, 'summary')
                  ? html2txt(getTag(item, 'summary').textContent)
                  : '';

                /** Collect content from entry */
                allContent = `${allContent} ${content}`;

                return {
                  id: getTag(item, 'id') ? getTag(item, 'id').textContent : null,
                  date: dateString,
                  href: getTag(item, 'link') ? getTag(item, 'link').getAttribute('href') : '',
                  title: getTag(item, 'title') ? getTag(item, 'title').innerHTML : '',
                  content,
                };
              })
              .filter(item => item.id !== null);

            dispatch({
              type: types.GET_FEED_SUCCESS,
              payload: {
                feed,
                mostFrequentWords: getMostFrequentWords(allContent),
              },
            });
          }

          resolve();
        } catch (error) {
          dispatch({
            type: types.GET_FEED_FAIL,
            payload: {
              error,
            },
          });

          reject(error);
        }
      })
      .catch(res => {
        dispatch({
          type: types.GET_FEED_FAIL,
        });
        reject(res);
      });
  });
};
