require('dotenv').config();
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const errorhandler = require('errorhandler');
const mongoose = require('mongoose');

mongoose.promise = global.Promise;

const app = express();
const port = process.env.SERVER_PORT || 1429;

/** Normal express config defaults */
app.use(require('morgan')('dev'));

app.use(bodyParser.json());
app.use(
  session({
    secret: 'mintos',
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: false,
  }),
);

if (process.env.NODE_ENV !== 'production') {
  app.use(errorhandler());
}

/** Configure Mongoose and passport */
mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
});
mongoose.set('debug', true);
require('./models/Users');
require('./config/passport');

app.use(require('./routes'));

/** catch 404 and forward to error handler */
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/** development error handler will print stacktrace */
if (process.env.NODE_ENV !== 'production') {
  /* eslint-disable no-unused-vars */
  app.use((err, req, res, next) => {
    /* eslint-enable no-unused-vars */
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.listen(port, () => console.log(`App listening on port ${port}!`));

/** production error handler no stack traces leaked to user */
/* eslint-disable no-unused-vars */
app.use((err, req, res, next) => {
  /* eslint-enable no-unused-vars */
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: err,
    },
  });
});
