const router = require('express').Router();
const request = require('request-promise-native');
const auth = require('../auth');

/**
 * Get feed
 * @method GET
 * @access private
 */

router.get('/', auth.required, async (req, res) => {
  try {
    const options = {
      method: 'GET',
      uri: 'https://www.theregister.co.uk/software/headlines.atom',
    };

    const response = await request(options);
    res.json({
      feed: response,
    });
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
