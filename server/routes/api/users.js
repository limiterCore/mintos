const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../auth');

const Users = mongoose.model('Users');

/**
 * Registration
 * @method POST
 * @access public
 */

/* eslint-disable no-unused-vars */
router.post('/', auth.optional, async (req, res, next) => {
  /* eslint-enable no-unused-vars */
  const {
    body: { user },
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  const found = await Users.findOne({ email: user.email });
  if (found) {
    return res.status(422).json({
      errors: {
        'such email': 'has been already registered',
      },
    });
  }

  const finalUser = new Users(user);
  finalUser.setPassword(user.password);
  return finalUser.save().then(() => res.json({ user: finalUser.toAuthJSON() }));
});

/**
 * Check email
 * @method POST
 * @access public
 */

/* eslint-disable no-unused-vars */
router.post('/check', auth.optional, async (req, res, next) => {
  /* eslint-enable no-unused-vars */
  const {
    body: { email },
  } = req;

  if (!email) {
    return res.json({
      result: false,
    });
  }

  const found = await Users.findOne({ email });
  if (found) {
    return res.json({
      result: true,
    });
  }

  return res.json({
    result: false,
  });
});

/**
 * Login
 * @method POST
 * @access public
 */
router.post('/login', auth.optional, (req, res, next) => {
  const {
    body: { user },
  } = req;

  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
    if (err) {
      return next(err);
    }

    if (passportUser) {
      const userToReturn = passportUser;
      userToReturn.token = passportUser.generateJWT();

      return res.json({ user: userToReturn.toAuthJSON() });
    }

    return res.status(422).json(info);
  })(req, res, next);
});

/**
 * Get current user
 * @method GET
 * @access private
 */
router.get(
  '/current',
  auth.required,
  /* eslint-disable no-unused-vars */
  (req, res, next) => {
    const {
      payload: { id },
    } = req;

    return Users.findById(id).then(user => {
      if (!user) {
        return res.sendStatus(400);
      }

      return res.json({ user: user.toAuthJSON() });
    });
  },
);

module.exports = router;
